# frozen_string_literal: true

class LeaderboardService
  def initialize(leaderboard:)
    @leaderboard = leaderboard
  end

  def ranking
    @ranking ||= leaderboard.entries.group(:username).order('sum_score DESC').sum(:score)
  end

  def position(username:)
    index = ranking.find_index { |key, _| key == username }
    return unless index

    index + 1
  end

  private

  attr_reader :leaderboard
end

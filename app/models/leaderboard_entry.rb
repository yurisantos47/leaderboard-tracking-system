# frozen_string_literal: true

class LeaderboardEntry < ApplicationRecord
  belongs_to :leaderboard

  validates :score, numericality: true
end

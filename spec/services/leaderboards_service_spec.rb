# frozen_string_literal: true

require 'rails_helper'

describe LeaderboardService do
  let(:leaderboard) { Leaderboard.create(name: 'Leaderboard') }

  subject { described_class.new(leaderboard: leaderboard) }

  before do
    leaderboard.entries << LeaderboardEntry.new(username: 'John', score: 5)
    leaderboard.entries << LeaderboardEntry.new(username: 'Mary', score: 10)
    leaderboard.entries << LeaderboardEntry.new(username: 'Mike', score: 15)
  end

  it 'returns leaderboards ranking' do
    expect(subject.ranking).to eq('Mike' => 15, 'Mary' => 10, 'John' => 5)
  end

  context 'position' do
    context 'when username is included in the leaderboard' do
      it "returns username's position" do
        expect(subject.position(username: 'John')).to eq(3)
        expect(subject.position(username: 'Mary')).to eq(2)
        expect(subject.position(username: 'Mike')).to eq(1)
      end
    end

    context 'when user name is not included in the leaderboard' do
      it 'returns nil' do
        expect(subject.position(username: 'Grace')).to be_nil
      end
    end
  end
end

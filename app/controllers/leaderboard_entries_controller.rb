# frozen_string_literal: true

class LeaderboardEntriesController < ApplicationController
  before_action :set_leaderboard_entry, only: %i[destroy]
  before_action :set_leaderboard, only: %i[index destroy]

  # GET /leaderboard_entries
  def index
    @leaderboard = Leaderboard.find(params[:leaderboard_id])
    @leaderboard_entries = LeaderboardEntry.includes(:leaderboard).where(username: params[:username])
  end

  # DELETE /leaderboard_entries/1
  def destroy
    @leaderboard_entry.destroy
    redirect_to @leaderboard, notice: 'Leaderboard entry was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_leaderboard_entry
    @leaderboard_entry = LeaderboardEntry.find(params[:id])
  end

  def set_leaderboard
    @leaderboard = Leaderboard.find(params[:leaderboard_id])
  end

  # Only allow a trusted parameter "white list" through.
  def leaderboard_entry_params
    params.require(:leaderboard_entry).permit(:leaderboard_id, :username, :score)
  end
end

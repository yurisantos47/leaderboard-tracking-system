# frozen_string_literal: true

require 'rails_helper'

describe UsersRankingService do
  let(:leaderboard) { Leaderboard.create(name: 'Leaderboard') }
  let(:username) { 'John' }

  subject { UsersRankingService.new(username: username, leaderboard: leaderboard) }

  context '#initialize' do
    it 'sets current position based on the leaderboard ranking' do
      allow_any_instance_of(LeaderboardService).to receive(:position).with(username: username).and_return(42)
      expect(subject.current_position).to eq(42)
    end
  end

  context '#add_score' do
    it 'return false when it fails to add a score' do
      allow_any_instance_of(LeaderboardEntry).to receive(:save).and_return(false)
      expect(subject.add_score(10)).to eq(false)
    end

    it 'return true when it succeeds to add a score' do
      allow_any_instance_of(LeaderboardEntry).to receive(:save).and_return(true)
      expect(subject.add_score(10)).to eq(true)
    end

    context 'when user does not have any scores in the leaderboard' do
      it 'adds a leaderboard entry' do
        expect { subject.add_score(10) }.to change { leaderboard.entries.count }.by(1)
        expect(leaderboard.entries.last.username).to eq(username)
        expect(leaderboard.entries.last.score).to eq(10)
      end

      it 'sets position gained as nil' do
        expect { subject.add_score(10) }.not_to change { subject.position_gained }
        expect(subject.position_gained).to be_nil
      end

      it 'updates current position based on the leaderboard ranking' do
        expect { subject.add_score(10) }.to change { subject.current_position }.from(nil)
      end
    end

    context 'when user already have scores in the leaderboard' do
      before do
        leaderboard.entries << LeaderboardEntry.new(username: username, score: 10)
        leaderboard.entries << LeaderboardEntry.new(username: 'Mary', score: 15)
      end

      it 'adds a leaderboard entry' do
        expect { subject.add_score(10) }.to change { leaderboard.entries.count }.by(1)
        expect(leaderboard.entries.last.username).to eq(username)
        expect(leaderboard.entries.last.score).to eq(10)
      end

      it 'sets position gained' do
        expect { subject.add_score(10) }.to change { subject.position_gained }.from(nil).to(1)
      end

      it 'updates current position based on the leaderboard ranking' do
        expect { subject.add_score(10) }.to change { subject.current_position }.from(2).to(1)
      end
    end
  end

  context 'position_gained_text' do
    context 'when position gained is nil' do
      before do
        allow(subject).to receive(:position_gained).and_return(nil)
        allow(subject).to receive(:current_position).and_return(3)
      end

      it { expect(subject.position_gained_text).to eq('John is on position 3.') }
    end

    context 'when position gained is positive' do
      before { allow(subject).to receive(:position_gained).and_return(3) }

      it { expect(subject.position_gained_text).to eq('John gained 3 position(s).') }
    end

    context 'when position gained is negative' do
      before { allow(subject).to receive(:position_gained).and_return(-3) }

      it { expect(subject.position_gained_text).to eq('John lost 3 position(s).') }
    end
  end
end

# frozen_string_literal: true

class UsersRankingService
  attr_reader :position_gained, :current_position

  def initialize(username:, leaderboard:)
    @username = username
    @leaderboard = leaderboard
    @current_position = LeaderboardService.new(leaderboard: leaderboard).position(username: username)
  end

  def add_score(score)
    entry = leaderboard.entries.build(username: username, score: score)

    return false unless entry.save

    set_user_positions

    true
  end

  def position_gained_text
    return "#{username} is on position #{current_position}." if position_gained.nil? || position_gained.zero?
    
    if position_gained.positive?
      "#{username} gained #{position_gained} position(s)."
    else
      "#{username} lost #{position_gained.abs} position(s)."
    end
  end

  private

  def set_user_positions
    new_position = LeaderboardService.new(leaderboard: leaderboard).position(username: username)

    @position_gained = current_position - new_position if current_position
    @current_position = new_position
  end

  attr_reader :username, :leaderboard
end

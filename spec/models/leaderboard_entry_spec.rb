# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LeaderboardEntry, type: :model do
  subject(:leaderboard_entry) { described_class.new(leaderboard: Leaderboard.new) }

  context 'validations' do
    context 'scores' do
      it 'returns error when value is nil' do
        leaderboard_entry.score = nil
        expect(leaderboard_entry).to be_invalid
        expect(leaderboard_entry.errors[:score]).to include('is not a number')
      end

      it 'validates when value is a number' do
        leaderboard_entry.score = 0
        expect(leaderboard_entry).to be_valid
      end
    end
  end
end

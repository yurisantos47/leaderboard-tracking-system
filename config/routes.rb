# frozen_string_literal: true

Rails.application.routes.draw do
  resources :leaderboards do
    resources :leaderboard_entries, only: %i[index destroy]
    post :add_score, on: :member
  end

  root to: 'leaderboards#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

# frozen_string_literal: true

class AddNotNullConstraintToScores < ActiveRecord::Migration[5.1]
  def change
    change_column_null(:leaderboard_entries, :score, false, 0)
  end
end
